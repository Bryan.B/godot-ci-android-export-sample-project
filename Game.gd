extends VBoxContainer

func _ready():
    $BackgroundMusic.play()
    $BackgroundMusicButton.connect("pressed", self, "on_background_music_button_pressed")
    $SoundEffectsButton.connect("pressed", self, "on_sound_effects_button_pressed")

func on_background_music_button_pressed():
    if $BackgroundMusicButton.pressed:
        $BackgroundMusic.play()
        $BackgroundMusicButton.text = "Music on"
        $BackgroundMusicButton.icon = preload("res://assets/icons/game-icons.net/sound-on.png")
    else:
        $BackgroundMusic.stop()
        $BackgroundMusicButton.text = "Music off"
        $BackgroundMusicButton.icon = preload("res://assets/icons/game-icons.net/sound-off.png")
    possibly_play_click_sound()

func on_sound_effects_button_pressed():
    if $SoundEffectsButton.pressed:
        $SoundEffectsButton.text = "Sounds on"
        $SoundEffectsButton.icon = preload("res://assets/icons/game-icons.net/speaker.png")
    else:
        $SoundEffectsButton.text = "Sounds off"
        $SoundEffectsButton.icon = preload("res://assets/icons/game-icons.net/speaker-off.png")
    possibly_play_click_sound()
        
func possibly_play_click_sound():
    if $SoundEffectsButton.pressed:
        $ClickSoundEffect.play()
